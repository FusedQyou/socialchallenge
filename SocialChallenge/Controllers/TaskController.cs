﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SocialChallenge.Methods;

using Data;
using Data.DataForms;
using static Data.Constants;

// Data Access libraries
using static DataLibrary.Logic.TaskProcessor;
using static DataLibrary.Logic.TeamProcessor;

// Main model
using TaskModel = Data.DataForms.Task;
using TeamModel = Data.DataForms.Team;

// Client model
using ClientTaskModel = Data.CreateForms.Task;

namespace SocialChallenge.Controllers
{
    public class TaskController : BaseController
    {
        #region Index
        // GET: Task/Index
        public ActionResult Index()
        {
            if (!UserSession.IsLoggedIn() || UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze pagina.";
                return RedirectToAction("Index", "Home");
            }

            List<TeamModel> teams = new List<TeamModel>();

            try
            {
                teams = LoadTeams();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong receiving all teams. {exception.Message}";
            }

            return View(teams);
        }
        #endregion

        #region Create
        // GET: Task/Create
        public ActionResult Create(int id)
        {
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "Je moet ingelogd zijn om een team aan te kunnen passen!";
                return RedirectToAction("Index", "Team");
            }

            if (UserSession.GetAccessLevel() != AccessLevel.Normal)
            {
                ErrorMessage = "Alleen leerlingen mogen taken aanmaken.";
                return RedirectToAction("Index", "Team");
            }

            if (UserSession.GetTeamId() != id)
            {
                ErrorMessage = "Je mag alleen taken voor je eigen team aanmaken!";
                return RedirectToAction("Index", "Team");
            }

            return View();
        }

        // POST: Task/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClientTaskModel task)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            try
            {
                CreateTask((int)UserSession.GetTeamId(), task.Name, task.Description);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong updating the new team. {exception.Message}";
                return View();
            }

            SuccessMessage = "Taak aangemaakt. Succes!";
            return RedirectToAction("Details", "Team", new { id = UserSession.GetTeamId() });
        }
        #endregion

        #region Approve / reject / revert
        // GET: Team/id/appove/taskid
        public ActionResult Approve(int id, int taskid)
        {
            if (!TaskApprovalHandler(taskid, Constants.TaskStatus.Approved))
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Details", "Team", new { id });
        }

        // GET: Team/id/reject/taskid
        public ActionResult Reject(int id, int taskid)
        {
            if (!TaskApprovalHandler(taskid, Constants.TaskStatus.Rejected))
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Details", "Team", new { id });
        }

        // GET: Team/id/reject/taskid
        public ActionResult Revert(int id, int taskid)
        {
            if (!TaskApprovalHandler(taskid, Constants.TaskStatus.New))
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Details", "Team", new { id });
        }

        private bool TaskApprovalHandler(int taskid, Constants.TaskStatus status)
        {
            // User must be logged in for this action
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "Je moet ingelogd zijn voor deze actie!";
                return false;
            }

            // User must be a teacher or admin
            if (UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent onbevoegd voor deze actie!";
                return false;
            }

            // Check if we are allowed to update the task
            TaskModel task = new TaskModel();
            task = GetTasksById(taskid);

            if (task.Approved == status)
            {
                ErrorMessage = $@"Deze taak staat al op status ""{status}""!";
                return true;
            }

            if (task.Done == TaskProgress.Open && status == Constants.TaskStatus.Approved)
            {
                ErrorMessage = "Deze taak is not niet volbracht door het team en mag nog niet goedgekeurd worden!";
                return true;
            }

            try
            {
                SetTaskApprovalById(taskid, status);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong reverting the task. {exception.Message}";
                return true;
            }

            SuccessMessage = $"Taak is {(status == Constants.TaskStatus.New ? "teruggezet" : status == Constants.TaskStatus.Approved ? "goedgekeurd" : "afgekeurd")}.";
            return true;
        }
        #endregion

        #region Edit
        // GET: Team/id/Edit/taskid
        public ActionResult Edit(int id, int? taskid)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een taak aan te passen!";
                return RedirectToAction("Index", "Home");
            }

            User user = Session["user"] as User;

            // Tasks may only be edited by those in that team
            if (UserSession.GetAccessLevel() != AccessLevel.Normal || user.TeamId != id)
            {
                ErrorMessage = "Alleen teamleden mogen hun taken aanpassen!";
                return RedirectToAction("Index", "Team");
            }

            if (taskid == null)
            {
                ErrorMessage = "Er is een taaknummer nodig om op te zoeken!";
                return RedirectToAction("Details", "Team", new { id });
            }

            TaskModel task = new TaskModel();
            task = GetTasksById((int)taskid);

            if (task == null)
            {
                ErrorMessage = "Er bestaat geen taak onder dit nummer";
                return RedirectToAction("Details", "Team", new { id });
            }

            // Tasks may only be edited if these are still open for approval, and not set as done
            if (task.Done == TaskProgress.Finished || task.Approved != Constants.TaskStatus.New)
            {
                ErrorMessage = "Taken mogen alleen aangepast worden als deze nog open staan!";
                return RedirectToAction("Details", "Team", new { id });
            }

            return View(task);
        }

        // POST: Team/id/Edit/taskid
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TaskModel task, int id, int taskid)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            try
            {
                UpdateTask(taskid, task.Name, task.Description);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when updating the task. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            SuccessMessage = "Taak aangepast!";
            return RedirectToAction("Details", "Team", new { id });
        }
        #endregion

        #region Finish
        // GET: Team/id/Finish/taskid
        public ActionResult Finish(int id, int? taskid)
        {
            // User must be logged in in order to finish a task
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een taak af te vinken!";
                return RedirectToAction("Index", "Home");
            }

            User user = Session["user"] as User;

            // Tasks may only be finished by those in that team
            if (UserSession.GetAccessLevel() != AccessLevel.Normal || user.TeamId != id)
            {
                ErrorMessage = "Alleen teamleden mogen hun taken afvinken!";
                return RedirectToAction("Index", "Team");
            }

            if (taskid == null)
            {
                ErrorMessage = "Er is een taaknummer nodig om een taak af te kunnen vinken!";
                return RedirectToAction("Details", "Team", new { id });
            }

            TaskModel task = new TaskModel();
            task = GetTasksById((int)taskid);

            if (task == null)
            {
                ErrorMessage = "Er bestaat geen taak onder dit nummer";
                return RedirectToAction("Details", "Team", new { id });
            }

            // Tasks may only be finished if these are still open for approval, and not set as done
            if (task.Done == TaskProgress.Finished || task.Approved != Constants.TaskStatus.New)
            {
                ErrorMessage = "Taken mogen alleen afgevinkt worden als deze open staan!";
                return RedirectToAction("Details", "Team", new { id });
            }

            try
            {
                SetTaskFinished((int)taskid);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when finishing the task. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            SuccessMessage = "Deze taak is afgevinkt! Nu afwachten tot een docent deze goed- of af-keurt.";
            return RedirectToAction("Details", "Team", new { id });
        }
        #endregion

        #region Revenuechange
        // GET: Team/id/Revenuechange
        public ActionResult Revenuechange(int id)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om de opbrengst aan te passen!";
                return RedirectToAction("Index", "Home");
            }

            User user = Session["user"] as User;

            // Tasks may only be edited by those in that team
            if (UserSession.GetAccessLevel() != AccessLevel.Normal || user.TeamId != id)
            {
                ErrorMessage = "Alleen teamleden mogen hun opbrengst aanpassen!";
                return RedirectToAction("Index", "Home");
            }

            TeamModel team = new TeamModel();
            team = LoadTeamByTeamId(id);

            if (team == null)
            {
                ErrorMessage = "Er bestaat geen team onder dit nummer";
                return RedirectToAction("Index", "Home");
            }

            return View(team);
        }

        // POST: Team/id/Edit/taskid
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Revenuechange(TeamModel team, int id)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            try
            {
                UpdateTeamRevenue(id, team.Revenue);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when updating the team revenue. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            SuccessMessage = "Totale opbrengst aangepast!";
            return RedirectToAction("Details", "Team", new { id });
        }
        #endregion
    }
}