﻿using System;
using System.Web.Mvc;

namespace SocialChallenge.Controllers
{
    public abstract class BaseController : Controller
    {
        public String InfoMessage
        {
            get { return GetAlerter("InfoMessage"); }
            set { SetAlerter("InfoMessage", value); }
        }

        public String SuccessMessage
        {
            get { return GetAlerter("SuccessMessage"); }
            set { SetAlerter("SuccessMessage", value); }
        }

        public String ErrorMessage
        {
            get { return GetAlerter("ErrorMessage"); }
            set { SetAlerter("ErrorMessage", value); }
        }

        private string GetAlerter(string index)
            => TempData[index] == null ? String.Empty : TempData[index].ToString();

        private void SetAlerter (string index, string message)
            => TempData[index] = message;
    }
}