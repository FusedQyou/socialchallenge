﻿using System.Web.Mvc;

// Data Access libraries
using static DataLibrary.Logic.UserProcessor;

// Main model
using UserModel = Data.DataForms.User;

// Client model
using ClientUserModel = Data.CreateForms.User;

namespace SocialChallenge.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        // GET: Profile
        public ActionResult Profile(int? id, string referral)
        {
            // An id is required for the controller method to work
            if (id == null)
            {
                ErrorMessage = "Profiel vereist een id van een user!";
                return RedirectToAction("Index", "User");
            }

            UserModel user = new UserModel();

            // Referral link
            ViewBag.referral = referral;

            // Load the user
            user = LoadUserById((int)id);

            // If user id is 0 nothing was returned
            if (user == null)
            {
                ErrorMessage = "Gebruiker bestaat niet!";
                return RedirectToAction("Index", "User");
            }

            return View(user);
        }
    }
}