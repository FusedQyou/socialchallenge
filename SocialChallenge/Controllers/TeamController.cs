﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SocialChallenge.Methods;

using static Data.Constants;

// Data Access libraries
using static DataLibrary.Logic.TeamProcessor;

// Main model
using TeamModel = Data.DataForms.Team;
using UserModel = Data.DataForms.User;

// Client model
using ClientTeamModel = Data.CreateForms.Team;

namespace SocialChallenge.Controllers
{
    public class TeamController : BaseController
    {
        #region Index
        // GET: User/
        public ActionResult Index()
        {
            if (!UserSession.IsLoggedIn() || UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze pagina.";
                return RedirectToAction("Index", "Home");
            }

            List<TeamModel> teams = new List<TeamModel>();

            try
            {
                teams = LoadTeams();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred receiving all teams. {exception.Message}";
            }

            return View(teams);
        }
        #endregion

        #region Details
        // GET: Recipe/Details/id
        public ActionResult Details(int? id)
        {
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "Je moet ingelogd zijn om een teampagina te bekijken.";
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                ErrorMessage = "Een nummer moet gegeven worden.";
                return RedirectToAction("index", "Home");
            }

            if (UserSession.GetAccessLevel() == AccessLevel.Normal && UserSession.GetTeamId() != id)
            {
                ErrorMessage = "Je bent niet bevoegt om dit team te zien.";
                return RedirectToAction("Index", "Home");
            }

            TeamModel team = new TeamModel();

            try
            {
                team = LoadTeamByTeamId((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred receiving the team. {exception.Message}";
                return RedirectToAction("index");
            }

            if (team == null)
            {
                ErrorMessage = "Er bestaat geen team onder dit nummer.";
                return RedirectToAction("index");
            }

            return View(team);
        }
        #endregion

        #region Mine
        // GET: Recipe/Details/id
        public ActionResult Mine(int? id)
        {
            if (Session["user"] == null)
            {
                ErrorMessage = "Je moet ingelogd zijn om je team te kunnen vinden. Kies je team uit de lijst.";
                return RedirectToAction("index", "Team");
            }

            if (UserSession.GetAccessLevel() != AccessLevel.Normal)
            {
                ErrorMessage = "Alleen leerlingen kunnen in een team kunnen zitten en kunnen hun team vinden!";
                return RedirectToAction("Details", "Team", new { id });
            }

            if (id == null)
            {
                ErrorMessage = "Je teamnummer is nodig. Kies je team uit de lijst.";
                return RedirectToAction("index", "Team");
            }

            TeamModel team = new TeamModel();

            try
            {
                team = LoadTeamByUserId((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred receiving your team. {exception.Message}";
                return RedirectToAction("index");
            }

            if (team == null)
            {
                ErrorMessage = "Je zit nog niet in een team. Vraag een docent of hij/zij jou in een team toe kan voegen.";
                return RedirectToAction("index");
            }

            return RedirectToAction("Details", "Team", new { team.Id });
        }
        #endregion

        #region Edit
        // GET: Team/Edit/id
        public ActionResult Edit(int? id)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "Je moet ingelogd zijn om je team aan te kunnen passen!";
                return RedirectToAction("index", "Home");
            }

            if (UserSession.GetAccessLevel() != AccessLevel.Normal)
            {
                ErrorMessage = "Alleen leerlingen kunnen in een team kunnen zitten en kunnen hun team aanpassen!";
                return RedirectToAction("Details", "Team", new {id});
            }

            // Id must exist
            if (id == null)
            {
                ErrorMessage = "Je teamid is nodig!";
                return RedirectToAction("index", "Home");
            }

            // Normal users may only edit his own team
            if (UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                int? teamId = (Session["user"] as UserModel).TeamId;

                if (teamId == null)
                {
                    ErrorMessage = "Je zit niet in een team, en mag andere teams ook niet aanpassen!";
                    return RedirectToAction("index");
                }

                if (teamId != id)
                {
                    ErrorMessage = "Je mag alleen je eigen team aanpassen!";
                    return RedirectToAction("index");
                }
            }

            TeamModel teamModel = new TeamModel();

            try
            {
                teamModel = LoadTeamByTeamId((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when receiving the team. {exception.Message}";
                return RedirectToAction("index");
            }

            if (teamModel == null)
            {
                ErrorMessage = "Er bestaat geen team onder dit nummer!";
                return RedirectToAction("index");
            }

            return View(teamModel);
        }

        // POST: User/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TeamModel team)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            try
            {
                UpdateTeam(team.Id, team.Name, team.Charitygoal);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong updating the team. {exception.Message}";
                return View();
            }

            SuccessMessage = "Je team is aangepast!";
            return RedirectToAction("Details", new { id = team.Id });
        }
        #endregion

        #region Delete
        // GET: Team/Delete/id
        public ActionResult Delete(int id)
        {
            // User must be logged in in order to delete a task
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een team te kunnen verwijderen!";
                return RedirectToAction("Index", "Home");
            }

            UserModel user = Session["user"] as UserModel;

            // Tasks may only be finished by those in that team
            if (UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze actie.";
                return RedirectToAction("Index", "Home");
            }

            TeamModel team = new TeamModel();

            try
            {
                team = LoadTeamByTeamId(id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when loading the task. {exception.Message}";
                return RedirectToAction("Index", "Team");
            }

            if (team == null)
            {
                ErrorMessage = "Er bestaat geen team onder dit nummer";
                return RedirectToAction("Index", "Team");
            }

            if (team.Available == false)
            {
                ErrorMessage = "Dit team is al verwijderd";
                return RedirectToAction("Details", "Team", new {id});
            }

            // Remove all users from the team
            try
            {
                SetUserTeamIdByTeamId(null, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when removing all users from the team. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            // Set the team as removed
            try
            {
                SetTeamAvailabilityByTeamId(false, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when setting the team to unavailable. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            SuccessMessage = "Team verwijderd.";
            return RedirectToAction("Details", "Team", new { id });
        }
        #endregion

        #region Recover
        // GET: Team/Recover/id
        public ActionResult Recover(int id)
        {
            // User must be logged in in order to delete a task
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een team te kunnen herstellen!";
                return RedirectToAction("Index", "Home");
            }

            UserModel user = Session["user"] as UserModel;

            // Tasks may only be finished by those in that team
            if (UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze actie.";
                return RedirectToAction("Index", "Home");
            }

            TeamModel team = new TeamModel();

            try
            {
                team = LoadTeamByTeamId(id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when loading the task. {exception.Message}";
                return RedirectToAction("Index", "Team");
            }

            if (team == null)
            {
                ErrorMessage = "Er bestaat geen team onder dit nummer";
                return RedirectToAction("Index", "Team");
            }

            if (team.Available == true)
            {
                ErrorMessage = "Dit team is niet verwijderd!";
                return RedirectToAction("Details", "Team", new { id });
            }

            // Set the team as available
            try
            {
                SetTeamAvailabilityByTeamId(true, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when setting the team to available. {exception.Message}";
                return RedirectToAction("Details", "Team", new { id });
            }

            SuccessMessage = "Team hersteld.";
            return RedirectToAction("Details", "Team", new { id });
        }
        #endregion
    }
}