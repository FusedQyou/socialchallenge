﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using SocialChallenge.Methods;

using Data.DataForms;
using static Data.Constants;

// Data Access libraries
using static DataLibrary.Logic.CodeProcessor;
using static DataLibrary.Logic.TeamProcessor;

// Main model
using CodeModel = Data.DataForms.Code;

// Client model
using ClientCodeModel = Data.CreateForms.Code;

namespace SocialChallenge.Controllers
{
    public class CodeController : BaseController
    {
        #region Index
        // GET: Codes/
        public ActionResult Index()
        {
            if (!UserSession.IsLoggedIn() || UserSession.GetAccessLevel() != AccessLevel.Admin)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze pagina.";
                return RedirectToAction("Index", "Home");
            }

            List<CodeModel> codes = new List<CodeModel>();

            try
            {
                codes = GetCodes();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred receiving all codes. {exception.Message}";
            }

            return View(codes);
        }
        #endregion

        #region Create
        // POST: Codes/
        [HttpPost]
        [ActionName("Index")]
        public ActionResult CreateNewCode()
        {
            List<CodeModel> codes = new List<CodeModel>();

            try
            {
                codes = GetCodes();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred receiving all codes. {exception.Message}";
            }

            string teamString = Request.Form["teamid"];

            // teamid may only be digits
            if (!teamString.All(c => c >= '0' && c <= '9'))
            {
                ErrorMessage = "Teamid mag alleen uit een nummer bestaan!";
                return View(codes);
            }

            int? teamId = String.IsNullOrEmpty(teamString) ? (int?) null : Convert.ToInt32(teamString);
            AccessLevel level = Request.Form["teacher-code"] != null ? AccessLevel.Teacher : AccessLevel.Normal;

            string shareCode;
            try
            {
                shareCode = CreateCode(level, level == AccessLevel.Normal ? teamId : null);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong when creating a new code. {exception.Message}";
                return View();
            }

            // Add the new code to the list so it's viewed with the rest
            codes.Add(new Code {
                Id = shareCode,
                Access_level = level,
                TeamId = teamId,
                Use_By = DateTime.Now.AddDays(level == AccessLevel.Normal ? StudentCodeDayLimit : TeacherCodeDayLimit)
            });

            // Check if a team should be created
            bool newTeamCreated = false;
            if (teamId != null)
            {
                Team team = new Team();
                try
                {
                    team = LoadTeamByTeamId((int)teamId);
                }
                catch (Exception exception)
                {
                    ErrorMessage = $"Something went wrong when checking for existing team. {exception.Message}";
                    return View();
                }

                if (team == null)
                {
                    try
                    {
                        CreateTeam($"Team{teamId}", teamId);
                    }
                    catch (Exception exception)
                    {
                        ErrorMessage = $"Something went wrong when creating a team for the user. {exception.Message}";
                        return View();
                    }

                    newTeamCreated = true;
                }
            }

            SuccessMessage = $"De nieuwe code is aangemaakt! {(newTeamCreated ? "Er is ook een team aangemaakt. " : "")}<u>Geef deze aan de gebruiker om te registreren!</u><br/>De code kan je hieronder vinden.";
            InfoMessage = shareCode;
            return View(codes);
        }
        #endregion

        #region Show
        // POST: Codes/Show/id
        public ActionResult Show(string id)
        {
            if (!UserSession.IsLoggedIn() || UserSession.GetAccessLevel() != AccessLevel.Admin)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze pagina.";
                return RedirectToAction("Index", "Home");
            }

            return Content(id);
        }
        #endregion
    }
}