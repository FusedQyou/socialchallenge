﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SocialChallenge.Methods;

using Data;
using static Data.Constants;

// Data Access libraries
using static DataLibrary.Logic.UserProcessor;
using static DataLibrary.Logic.CodeProcessor;
using static DataLibrary.Logic.TeamProcessor;

// Main model
using UserModel = Data.DataForms.User;
using CodeModel = Data.DataForms.Code;
using TeamModel = Data.DataForms.Team;

// Client model
using ClientUserModel = Data.CreateForms.User;
using ClientTeamModel = Data.CreateForms.Team;

namespace SocialChallenge.Controllers
{
    public class UserController : BaseController
    {
        #region Index
        // GET: User/
        public ActionResult Index()
        {
            if (!UserSession.IsLoggedIn() || UserSession.GetAccessLevel() == AccessLevel.Normal)
            {
                ErrorMessage = "Je bent niet bevoegd voor deze pagina.";
                return RedirectToAction("Index", "Home");
            }

            List<UserModel> users = new List<UserModel>();

            try
            {
                users = LoadUsers();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred. {exception.Message}";
            }

            // Remove all users that have been removed when not admin
            if (Session["user"] == null || (Session["user"] as UserModel).Access_level != AccessLevel.Admin)
            {
                users = users.Where(user => user.Status != StatusType.Removed).ToList();
            }

            return View(users);
        }
        #endregion

        #region SignUp
        // GET: User/Signup
        public ActionResult SignUp()
        {
            // If you're already logged in there's no need to try and register
            if (UserSession.IsLoggedIn())
            {
                ErrorMessage = "Log uit voordat je een nieuwe registratie aanmaakt.";
                return RedirectToAction("index");
            }

            // Get the usercount for the registrationcode
            // No users means no registrationcode is needed the first time
            try
            {
                ViewBag.userCount = CountUsers();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong when grabbing total usercount. {exception.Message}";
                return View();
            }
            
            return View();
        }

        // POST: User/Signup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(ClientUserModel user)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            // Load all users beforehand. If we received 0, this is the first user and he will get full account access
            int? userCount;
            try
            {
                userCount = CountUsers();
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong when grabbing total usercount. {exception.Message}";
                return View();
            }

            // If a team is made for this user since there was none, this turns true.
            // It means that in the end, you will be prompted with another screen to set your team name
            bool firstTeamMember = false;
            string teamName = "";

            // Firstly confirm the registrationcode is valid when not the first signup. If it is we will receive the teamid and access_level
            // Set the accesslevel to admin by default. This is changed with the code, and it wont when no code is available, always making the first person admin
            AccessLevel accessLevel = AccessLevel.Admin;
            int? teamId = null;
            TeamModel team = new TeamModel();

            // If the team we load later is unavailable, this bool with turn true
            bool TeamIsUnavailable = false;

            if (userCount != 0)
            {
                if (user.Code == null)
                {
                    ErrorMessage = "Voer een geldige registratiecode in.";
                    return View();
                }

                CodeModel code = GetCodeById(user.Code);

                if (code == null)
                {
                    ErrorMessage = "Ongeldige registratiecode";
                    return View();
                }

                if (code.Use_By <= DateTime.Now)
                {
                    ErrorMessage = "Deze code kan niet meer gebruikt worden is ongeldig.";
                    return View();
                }

                // Maximum of 5 users can sign up with the same code
                if (code.Users != null && code.Users.Count >= 5)
                {
                    ErrorMessage = "Deze code is al 5 keer gebruikt en mag niet meer gebruikt worden.";
                    return View();
                }

                // Update the access level and teamid
                accessLevel = code.Access_level;
                teamId = code.TeamId;

                // The first registered teammember can change some team info if he wants
                if (teamId != null)
                {
                    try
                    {
                        team = LoadTeamByTeamId((int)teamId);
                    }
                    catch (Exception exception)
                    {
                        ErrorMessage = $"Something went wrong when checking the team for any existing users. {exception.Message}";
                        return View();
                    }

                    // If the signup is the first signup of the page, notify the user later on with this bool
                    if (team.Users.Count == 0 && team.Available)
                    {
                        firstTeamMember = true;
                    }

                    // If the team is unavailable/removed, set teamId to null and set a bool so we tell the user
                    if (!team.Available)
                    {
                        teamId = null;
                        TeamIsUnavailable = true;
                    }
                }
            }

            EncryptedPassword encryptedPassword = Password.HashAndSalt(user.Password);
            int lastId;

            try
            {
                lastId = CreateUser(user.Username, user.Firstname, user.Insertion, user.Lastname, encryptedPassword.HashedPassword, encryptedPassword.Salt, accessLevel, teamId);
            }
            catch (Exception exception)
            {
                // Check if the username is already in use, and inform the user
                if (exception.Message.Contains("Duplicate entry"))
                {
                    ErrorMessage = $@"Gebruiker ""{user.Username}"" bestaat al.";
                }
                else
                {
                    ErrorMessage = exception.Message;
                }

                return View();
            }

            // Add the user to the used code's userId's
            if (userCount != 0)
            {
                try
                {
                    AddUserIdtoCode(user.Code, lastId);
                }
                catch (Exception exception)
                {
                    ErrorMessage = $"Something went wrong when adding the user to the code's userIds. {exception.Message}";
                    return View();
                }
            }

            // Set the user in a session to log him in.
            Session["user"] = LoadUserById(lastId);

            string message = "Gebruiker aangemaakt en ingelogd!";

            // Notify the user he got admin access due to being the first signup
            if (userCount == 0)
            {
                message +=
                    "<br/>Omdat dit de eerst geregistreerde gebruiker is op de site, heeft deze admin toegangkelijkheid gekregen.";
            }

            // Notify the user the team is unavailable/removed
            if (TeamIsUnavailable)
            {
                message +=
                    "<br/>Het team gebonden aan deze code is helaas verwijderd. Vraag een docent voor een nieuw team.";
            }

            SuccessMessage = message;

            // If a new team was made for this user, we will load a tab for setting the team name, rather than redirecting to the previous page
            if (firstTeamMember)
            {
                return RedirectToAction("SetNewTeamInfo");
            }

            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Login
        // GET: User/Login
        public ActionResult Login()
        {
            // If you're already logged in there's no need to try again
            if (UserSession.IsLoggedIn())
            {
                ErrorMessage = "Je bent al ingelogd!";
                return RedirectToAction("index");
            }

            return View();
        }

        // POST: User/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ClientUserModel user)
        {
            string errormessage = "Ongeldig gebruikersnaam of wachtwoord.";

            // Get the user based on the username, and encrypt the password so we can compare those
            UserModel dbUser = LoadUserByUsername(user.Username);

            // No user returned
            if (dbUser == null)
            {
                ErrorMessage = errormessage;
                return View();
            }

            // Password doesn't match
            if (!Password.Confirm(user.Password, dbUser.PasswordHash, dbUser.Salt))
            {
                ErrorMessage = errormessage;
                return View();
            }

            // User has been removed
            if (dbUser.Status == StatusType.Removed)
            {
                ErrorMessage = "Deze gebruiker is reeds verwijderd en moet door een admin weer geactiveerd worden.";
                return View();
            }

            SuccessMessage = "Je bent ingelogd.";
            Session["user"] = dbUser;

            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Logout
        // POST: User/Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            SuccessMessage = "Je bent uitgelogd.";
            Session["user"] = null;
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Edit
        // GET: User/Edit/id
        public ActionResult Edit(int? id)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een profiel aan te passen!";
                return RedirectToAction("Index", "Home");
            }

            // Id must exist
            if (id == null)
            {
                ErrorMessage = "Je hebt de id van een gebruiker nodig om een profiel aan te passen!";
                return RedirectToAction("Index", "Home");
            }

            // User may only edit his own page
            if (UserSession.GetId() != id && UserSession.GetAccessLevel() != AccessLevel.Admin)
            {
                ErrorMessage = "Je kan alleen je eigen profiel aanpassen!";
                return RedirectToAction("Index", "Home");
            }

            UserModel user = new UserModel();

            try
            {
                user = LoadUserById((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when receiving the user. {exception.Message}";
                return View(user);
            }

            if (user.Id == 0)
            {
                ErrorMessage = $"Geen gebruiker onder id {id}.";
                return RedirectToAction("index");
            }

            return View(user);
        }

        // POST: User/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserModel user, int id)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View(user);
            }

            user.Id = id;

            if (user.Access_level != AccessLevel.Normal && user.TeamId != null)
            {
                ErrorMessage = "Alleen leerlingen kunnen een team aangewezen krijgen.";
                return View(user);
            }

            TeamModel team = new TeamModel();

            if (user.TeamId != null)
            {
                try
                {
                    team = LoadTeamByTeamId((int)user.TeamId);
                }
                catch (Exception exception)
                {
                    ErrorMessage = $"An error occurred receiving the team. {exception.Message}";
                    return View(user);
                }

                if (team == null)
                {
                    ErrorMessage = "Er bestaat geen team onder dit nummer.";
                    return View(user);
                }

                if (team.Available == false)
                {
                    ErrorMessage = "Dit team is niet meer beschikbaar.";
                    return View(user);
                }
            }

            try
            {
                UpdateUserByClass(user);
            }
            catch (Exception exception)
            {
                if (exception.Message.Contains("Duplicate entry"))
                {
                    ErrorMessage = $"Gebruiker {user.Username} bestaat al!";
                }
                else
                {
                    ErrorMessage = $"An error occurred when updating the user. {exception.Message}";
                }

                return View(user);
            }

            SuccessMessage = "Profiel aangepast!";
            return RedirectToAction("index");
        }
        #endregion

        #region Delete
        // GET: User/Delete/id
        public ActionResult Delete(int? id)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een profiel te kunnen verwijderen!";
                return RedirectToAction("Index", "Home");
            }

            // Id must exist
            if (id == null)
            {
                ErrorMessage = "Je hebt de id van een gebruiker nodig om een profiel te verwijderen!";
                return RedirectToAction("Index", "Home");
            }

            // User may only remove other users if he has the access level
            if (UserSession.GetAccessLevel() != AccessLevel.Admin)
            {
                ErrorMessage = "Je hebt geen toestemming om gebruikers te verwijderen!";
                return RedirectToAction("Index", "Home");
            }

            // User may not remove himself
            if (UserSession.GetId() == id)
            {
                ErrorMessage = "Je kan jezelf niet verwijderen!";
                return RedirectToAction("index");
            }

            UserModel user = new UserModel();

            try
            {
                user = LoadUserById((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when receiving the user. {exception.Message}";
                return View(user);
            }

            // If the user is already removed or none was received, return an error
            if (user.Id == 0 || user.Id != 0 && user.Status == StatusType.Removed)
            {
                ErrorMessage = $"Geen gebruiker onder id {id}.";
                return RedirectToAction("index");
            }

            return View(user);
        }

        // POST: User/Delete/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(UserModel user, int id)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return RedirectToAction("index");
            }

            try
            {
                SetUserStatusById(StatusType.Removed, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred removing the user. {exception.Message}";
                return View();
            }

            try
            {
                SetUserTeamById(null, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred removing the user's team. {exception.Message}";
                return View();
            }

            SuccessMessage = "Gebruiker verwijderd.";
            return RedirectToAction("index");
        }
        #endregion

        #region Recover
        // GET: User/Delete/id
        public ActionResult Recover(int? id)
        {
            // User must be logged in
            if (!UserSession.IsLoggedIn())
            {
                ErrorMessage = "U moet ingelogd zijn om een profiel te kunnen herstellen!";
                return RedirectToAction("Index", "Home");
            }

            // Id must exist
            if (id == null)
            {
                ErrorMessage = "Je hebt de id van een gebruiker nodig om een profiel te herstellen!";
                return RedirectToAction("Index", "Home");
            }

            // User may only recover other users if he has the access level
            if (UserSession.GetAccessLevel() != AccessLevel.Admin)
            {
                ErrorMessage = "Je hebt geen toestemming om gebruikers te herstellen!";
                return RedirectToAction("Index", "Home");
            }

            UserModel user = new UserModel();

            try
            {
                user = LoadUserById((int)id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred when receiving the user. {exception.Message}";
                return View(user);
            }

            // If none was received, return an error
            if (user.Id == 0)
            {
                ErrorMessage = $"Geen gebruiker onder id {id}.";
                return RedirectToAction("index");
            }

            // If the user is not removed, notify the user
            if (user.Status == StatusType.Normal)
            {
                ErrorMessage = "Deze gebruiker hoeft niet hersteld te worden!";
                return RedirectToAction("index");
            }

            return View(user);
        }

        // POST: User/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Recover(UserModel user, int id)
        {
            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return RedirectToAction("index");
            }

            try
            {
                SetUserStatusById(StatusType.Normal, id);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"An error occurred recovering the user. {exception.Message}";
                return View();
            }

            SuccessMessage = "Gebruiker hersteld.";
            return RedirectToAction("index");
        }
        #endregion

        #region SetNewTeamInfo
        // GET: User/SetNewTeamInfo
        public ActionResult SetNewTeamInfo()
        {
            if (Session["user"] == null)
            {
                ErrorMessage = "Je moet ingelogd zijn.";
                return RedirectToAction("Index", "Home");
            }
            
            TeamModel teamModel = new TeamModel();
            teamModel = LoadTeamByUserId((Session["user"] as UserModel).Id);

            // Redirect to the proper edit page if this page was already accessed once before by this team.
            if (teamModel.FirstTimeNameChangeDone)
            {
                return RedirectToAction("edit", "Team", new {teamModel.Id});
            }

            // Indicate this page was accessed once, which means it shouln't be used again.
            UpdateFirstTeamNameChange(teamModel.Id, true);

            ClientTeamModel team = new ClientTeamModel
            {
                Name = teamModel.Name
            };

            return View(team);
        }

        // POST: User/SetNewTeamInfo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetNewTeamInfo(ClientTeamModel team)
        {
            int teamId = (Session["user"] as UserModel).Team.Id;

            if (!ModelState.IsValid)
            {
                ErrorMessage = "Form is niet geldig";
                return View();
            }

            try
            {
                UpdateTeam(teamId, team.Name, team.Charitygoal);
            }
            catch (Exception exception)
            {
                ErrorMessage = $"Something went wrong updating the new team. {exception.Message}";
                return View();
            }

            SuccessMessage = "Welkom in je nieuwe team!";
            return RedirectToAction("Details", "Team", new {id = teamId});
        }
        #endregion
    }
}