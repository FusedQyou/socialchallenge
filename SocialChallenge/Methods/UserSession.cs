﻿using System.Web;

using Data.DataForms;
using static Data.Constants;

namespace SocialChallenge.Methods
{
    public static class  UserSession
    {
        // Method to get the current session
        private static User GetUserSession()
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                return HttpContext.Current.Session["user"] as User;
            }

            return null;
        }

        // Returns true if the user is logged in
        public static bool IsLoggedIn()
        {
            return GetUserSession() != null;
        }

        // Method to get the user's session username
        public static string GetUsername()
        {
            return GetUserSession().Username;
        }

        public static string GetFullName()
        {
            return GetUserSession().Fullname;
        }

        // Method to get the user's session id
        public static int GetId()
        {
            return GetUserSession().Id;
        }

        // Method to get the user's accessibility level
        public static AccessLevel GetAccessLevel()
        {
            return GetUserSession().Access_level;
        }

        // Method to get the user's status
        public static StatusType GetStatus()
        {
            return GetUserSession().Status;
        }

        // Method to get the user's team id
        public static int? GetTeamId()
        {
            return GetUserSession().Team != null ? GetUserSession().Team.Id : (int?)null;
        }
    }
}