﻿using System;
using System.Linq;
using System.Text;
using Data;

namespace SocialChallenge.Methods
{
    public class EncryptedPassword
    {
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
    }

    public static class Password
    {
        // Method to hash a password
        public static EncryptedPassword HashAndSalt(string password)
        {
            string salt = Encrypt.GetRandomString(20, 30);
            return new EncryptedPassword
            {
                HashedPassword = Hash(password, salt),
                Salt = salt
            };
        }

        // Method to confirm two passwords depending on the salt
        public static bool Confirm(string password, string hashedPassword, string salt)
        {
            string saltedPassword = MakeStringWithSalt(password, salt);
            return Encrypt.CompareWithEncrypted(saltedPassword, hashedPassword);
        }

        // Method that does the hashing
        public static string Hash(string password, string salt)
        {
            return Encrypt.Make(
                MakeStringWithSalt(password, salt)
            );
        }

        public static string MakeStringWithSalt(string baseString, string salt)
        {
            byte[] baseBytes = Encoding.UTF8.GetBytes(baseString);
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            byte[] saltedValue = baseBytes.Concat(saltBytes).ToArray();
            return Convert.ToBase64String(saltedValue);
        }
    }
}