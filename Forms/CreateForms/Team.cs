﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.CreateForms
{
    public class Team
    {
        [Display(Name = "Teamnaam")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Zorg ervoor dat je Teamnaam tussen {1} en {2} characters is.")]
        [Required(ErrorMessage = "Voer een geldige teamnaam in.")]
        public string Name { get; set; }

        [Display(Name = "Bedragsdoel")]
        [Range(1000, Int32.MaxValue, ErrorMessage = "Geef een geldig doelbedrag van minimaal {1} euro.")]
        public decimal? Charitygoal { get; set; }
    }
}