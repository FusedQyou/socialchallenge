﻿using System.ComponentModel.DataAnnotations;

namespace Data.CreateForms
{
    public class Code
    {
        [Display(Name = "Account level")]
        [Range(1, 2, ErrorMessage = "Je statusnummer moet 1 voor normaal zijn, of 2 voor een docent.")]
        [Required(ErrorMessage = "Voer een geldig account level in die de code zal geven.")]
        public int access_level { get; set; }

        [Display(Name = "Team identificatienummer")]
        [Range(0, 50, ErrorMessage = "Je team id moet tussen {1} en {2} minuten zijn.")]
        public int? TeamId { get; set; }
    }
}