﻿using System.ComponentModel.DataAnnotations;

namespace Data.CreateForms
{
    public class Task
    {
        [Display(Name = "Naam")]
        [Required(ErrorMessage = "Voer een geldige taak in.")]
        public string Name { get; set; }

        [Display(Name = "Descriptie")]
        [StringLength(300, MinimumLength = 0, ErrorMessage = "Zorg ervoor dat je descriptie maximaal {2} characters is.")]
        public string Description { get; set; }
    }
}
