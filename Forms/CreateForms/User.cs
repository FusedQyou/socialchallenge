﻿using System.ComponentModel.DataAnnotations;

namespace Data.CreateForms
{
    public class User
    {
        [Display(Name = "Registratie code")]
        [StringLength(80, MinimumLength = 10, ErrorMessage = "De registratiecode is tussen {2} en {1} characters.")]
        public string Code { get; set; }

        [Display(Name = "Login naam")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Zorg ervoor dat je login naam tussen {1} en {2} characters is.")]
        [Required(ErrorMessage = "Voer een geldige login naam in.")]
        public string Username { get; set; }

        [Display(Name = "Voornaam")]
        [StringLength(50, ErrorMessage = "Zorg ervoor dat je naam maximaal {2} characters is.")]
        [Required(ErrorMessage = "Voer een geldige voornaam in.")]
        public string Firstname { get; set; }

        [Display(Name = "Tussenvoegsel")]
        [StringLength(20, ErrorMessage = "Zorg ervoor dat je tussenvoegsel maximaal {2} characters is.")]
        public string Insertion { get; set; }

        [Display(Name = "Achternaam")]
        [StringLength(80, ErrorMessage = "Zorg ervoor dat je achternaam maximaal {2} characters is.")]
        public string Lastname { get; set; }

        [Display(Name = "wachtwoord")]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "Zorg ervoor dat je wachtwoord tussen {1} en {2} characters is.")]
        [Required(ErrorMessage = "Voer een geldig gebruikers wachtwoord in.")]
        public string Password { get; set; }

        [Display(Name = "Bevestig wachtwoord")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Wachtwoord bevestiging komt niet overeen met het wachtwoord.")]
        public string ConfirmPassword { get; set; }

        // Full name
        public string Fullname
        {
            get
            {
                string returnString = Firstname;
                if (Insertion != null && Lastname != null)
                {
                    returnString += $" {Insertion} {Lastname}";
                }
                else if (Lastname != null)
                {
                    returnString += $" {Lastname}";
                }

                return returnString;
            }
        }
    }
}