﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Data
{
    public class Encrypt
    {
        public static string GetRandomString(int min, int max)
        {
            byte[] bytes = new byte[new Random().Next(min, max)];
            new RNGCryptoServiceProvider().GetNonZeroBytes(bytes);
            return Convert.ToBase64String(bytes);
        }

        public static string Make(string stringToEncrypt)
        {
            return Convert.ToBase64String(
                new SHA256Managed().ComputeHash(
                    Encoding.UTF8.GetBytes(stringToEncrypt)
            ));
        }

        public static bool CompareWithEncrypted(string unecrypted, string encrypted)
        {
            string newEncrypted = Make(unecrypted);
            return String.Equals(newEncrypted, encrypted);
        }
    }
}