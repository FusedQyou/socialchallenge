﻿namespace Data
{
    public static class Constants
    {
        // The statusType of the account. When removed, the account is not usable
        public enum StatusType { Normal, Removed };

        // The level of the account. The higher the level, the more accessibility the account has.
        public enum AccessLevel { Normal, Teacher, Admin };

        // The status of a task
        public enum TaskStatus { New, Approved, Rejected };

        // The progress of a task
        public enum TaskProgress { Open, Finished };

        // The amount of days a code is usable for teachers and students
        public const int TeacherCodeDayLimit = 2;
        public const int StudentCodeDayLimit = 5;
    }
}