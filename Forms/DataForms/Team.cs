﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static Data.Constants;

namespace Data.DataForms
{
    public class Team
    {
        public int Id { get; set; }

        [Display(Name = "Naam")]
        public string Name { get; set; }

        [Display(Name = "Doel")]
        public decimal? Charitygoal { get; set; }

        [Display(Name = "opbrengst")]
        public decimal? Revenue { get; set; }

        public bool FirstTimeNameChangeDone { get; set; }

        public bool Available { get; set; }

        [Display(Name = "Teamleden")]
        public List<User> Users { get; set; }

        [Display(Name = "Taken")]
        public List<Task> Tasks { get; set; }

        public List<Task> FinishedTasks
        {
            get => Tasks.Where(task => task.Done == TaskProgress.Finished).ToList();
        }

        public string EarnString
        {
            get
            {
                string totalEarnString = Revenue == null ? "€0" : $"€{Revenue}";
                if (Charitygoal != null)
                {
                    totalEarnString += $" / €{Charitygoal}";
                }

                return totalEarnString;
            }
        }
    }
}
