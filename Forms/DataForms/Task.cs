﻿using System.ComponentModel.DataAnnotations;
using static Data.Constants;

namespace Data.DataForms
{
    public class Task
    {
        public int Id { get; set; }

        public int TeamId { get; set; }

        [Display(Name = "taak")]
        public string Name { get; set; }

        [Display(Name = "descriptie")]
        public string Description { get; set; }

        [Display(Name = "klaar")]
        public TaskProgress Done { get; set; }

        [Display(Name = "goedgekeurd")]
        public TaskStatus Approved { get; set; }
    }
}
