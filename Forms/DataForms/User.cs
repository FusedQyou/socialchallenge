﻿using System;
using System.ComponentModel.DataAnnotations;
using static Data.Constants;

namespace Data.DataForms
{
    public class User
    {
        public int Id { get; set; }

        [Display(Name = "Gebruikersnaam")]
        public string Username { get; set; }

        public string Firstname { get; set; }
        public string Insertion { get; set; }
        public string Lastname { get; set; }

        public DateTime Date_added { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public AccessLevel Access_level { get; set; }
        public StatusType Status { get; set; }

        // Full name
        [Display(Name = "Naam")]
        public string Fullname
        {
            get
            {
                string returnString = Firstname;
                if (Insertion != null && Lastname != null)
                {
                    returnString += $" {Insertion} {Lastname}";
                }
                else if (Lastname != null)
                {
                    returnString += $" {Lastname}";
                }

                return returnString;
            }
        }
    }
}
