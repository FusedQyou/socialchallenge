﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Data.DataForms
{
    public class Code
    {
        public string Id { get; set; }
        public string UsedByIds { get; set; }

        [Display(Name = "Gebruik voor")]
        public DateTime Use_By { get; set; }

        [Display(Name = "Account level")]
        public Constants.AccessLevel Access_level { get; set; }

        [Display(Name = "Team identificatienummer")]
        public int? TeamId { get; set; }

        [Display(Name = "Gebruikt door")]
        public List<User> Users { get; set; }

        public string GetUseByDate()
        {
            return Use_By.ToString("dd MMMMM", new CultureInfo("nl-NL"));
        }
    }
}
