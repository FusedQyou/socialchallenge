﻿using System;
using System.Collections.Generic;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace DataLibrary.DataAccess
{
    public static class MySqlDataAccess
    {
        // Get the connectionstring from the web.config in the main project
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnection"].ConnectionString;
        }

        // Create an entry to the database, returns nothing
        #region public static void Create(string query, List<MySqlParameter> MySQLParameterList)
        public static void Create(string query, List<MySqlParameter> MySQLParameterList)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(GetConnectionString()))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddRange(MySQLParameterList.ToArray());
                    command.ExecuteReader();
                }
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }
        #endregion

        // Create an entry to the database
        #region public static int? CreateReturnLastId(string query, List<MySqlParameter> MySQLParameterList)
        public static int? CreateReturnLastId(string query, List<MySqlParameter> MySQLParameterList)
        {
            // Update the query and add a string to get the last returned id
            query += "SELECT last_insert_id();";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(GetConnectionString()))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddRange(MySQLParameterList.ToArray());
                    MySqlDataReader result = command.ExecuteReader();

                    // Return nothing if we didn't receive anything
                    if (!result.HasRows)
                    {
                        return null;
                    }

                    if (result.Read())
                    {
                        return result.GetInt32(0);
                    }
                }
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return null;
        }
        #endregion

        // Update an entry in the database, returns nothing
        #region MyRegion public static void Update(string query, List<MySqlParameter> MySQLParameterList)
        public static void Update(string query, List<MySqlParameter> MySQLParameterList)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(GetConnectionString()))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddRange(MySQLParameterList.ToArray());
                    command.ExecuteReader();
                }
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }
        #endregion

        // Load a table from the database and list the results through a lambda expression
        #region public static void Load(string query, List<MySqlParameter> MySQLParameterList, Action<MySqlDataReader> processor)
        public static void Load(string query, List<MySqlParameter> MySQLParameterList, Action<MySqlDataReader> processor)
        {
            using (MySqlConnection connection = new MySqlConnection(GetConnectionString()))
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddRange(MySQLParameterList.ToArray());

                try
                {
                    using (MySqlDataReader result = command.ExecuteReader())
                    {
                        if (!result.HasRows)
                        {
                            return;
                        }

                        while (result.Read())
                        {
                            processor(result);
                        }
                    }
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }

            }
        }
        #endregion
    }
}
