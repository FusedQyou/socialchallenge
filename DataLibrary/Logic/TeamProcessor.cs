﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

using static DataLibrary.DataAccess.MySqlDataAccess;

using static DataLibrary.Logic.UserProcessor;
using static DataLibrary.Logic.TaskProcessor;

using Data.DataForms;

namespace DataLibrary.Logic
{
    // Main logic for the team table
    public class TeamProcessor
    {
        // Create a new team for the database
        public static void CreateTeam(string name, int? id = null)
        {
            string query = "INSERT INTO `teams` (id, name) VALUES (@id, @name);";
            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@id", id));
            MySqlParameterList.Add(new MySqlParameter("@name", name));

            try
            {
                Create(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        public static List<Team> LoadTeams()
        {
            return TeamLoader();
        }

        public static Team LoadTeamByUserId(int userId)
        {
            return TeamLoader(userId: userId).FirstOrDefault();
        }

        public static Team LoadTeamByTeamId(int teamId)
        {
            return TeamLoader(teamId: teamId).FirstOrDefault();
        }

        private static List<Team> TeamLoader(int? userId = null, int? teamId = null)
        {
            string query = @"SELECT
	                            `teams`.*,
                                `users`.`id` AS 'userid',
                                `users`.`username`
                            FROM `teams`
                            LEFT JOIN `users` ON `users`.`team_id` = `teams`.`id`";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();

            if (userId != null)
            {
                query += " WHERE `users`.`id`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", userId));
            }

            else if (teamId != null)
            {
                query += " WHERE `teams`.`id`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", teamId));
            }

            List<Team> teams = new List<Team>();

            try
            {
                Load(query, MySqlParameterList, (MySqlDataReader reader) =>
                {
                    int id = reader.GetInt32("id");
                    string name = reader.GetString("name");
                    bool firstTimeNameChangeDone = reader.GetInt32("first_time_change_done") == 1;
                    bool available = reader.GetInt32("available") == 1;
                    decimal? charityGoal = reader.IsDBNull(reader.GetOrdinal("charitygoal")) ? (decimal?)null : reader.GetDecimal("charitygoal");
                    decimal? revenue = reader.IsDBNull(reader.GetOrdinal("total_revenue")) ? (decimal?)null : reader.GetDecimal("total_revenue");

                    int? userid = reader.IsDBNull(reader.GetOrdinal("userid")) ? (int?)null : reader.GetInt32("userid");
                    string username = reader.IsDBNull(reader.GetOrdinal("username")) ? null : reader.GetString("username");

                    // First make sure the team exists before adding the team
                    // If it doesn't, then it has to be made. Otherwise it can be ignores
                    int index = teams.FindIndex(team => team.Id == id);

                    if (index == -1)
                    {
                        teams.Add(new Team
                        {
                            Id = id,
                            Name = name,
                            FirstTimeNameChangeDone = firstTimeNameChangeDone,
                            Available = available,
                            Charitygoal = charityGoal,
                            Revenue = revenue,
                            Tasks = LoadTeamTasksById(id),
                            Users = new List<User>()
                        });

                        // Update the index for the first team member to be added
                        index = teams.Count - 1;
                    }

                    // Add the user
                    if (userid != null)
                    {
                        teams[index].Users.Add(LoadUserById((int)userid));
                    }
                });
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return teams;
        }

        // Update a team
        public static void UpdateTeam(int id, string name, decimal? charitygoal)
        {
            string query = "UPDATE `teams` SET `name`=@name, `charitygoal`=@charitygoal WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@name", name));
            MySqlParameterList.Add(new MySqlParameter("@charitygoal", charitygoal));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update a team's revenue
        public static void UpdateTeamRevenue(int id, decimal? revenue)
        {
            string query = "UPDATE `teams` SET `total_revenue`=@revenue WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@revenue", revenue));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update the bool that checks whether the first time teamname change has been done after registering
        public static void UpdateFirstTeamNameChange(int id, bool done)
        {
            string query = "UPDATE `teams` SET `first_time_change_done`=@done WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@done", Convert.ToInt32(done)));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Set all user team ids that matches the teamid
        public static void SetUserTeamIdByTeamId(int? id, int teamId)
        {
            string query = "UPDATE `users` SET `team_id`=@id WHERE `team_id`=@teamId";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@id", id));
            MySqlParameterList.Add(new MySqlParameter("@teamId", teamId));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Remove/Restore the team
        public static void SetTeamAvailabilityByTeamId(bool available, int teamId)
        {
            string query = "UPDATE `teams` SET `available`=@available WHERE `id`=@teamId";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@available", available));
            MySqlParameterList.Add(new MySqlParameter("@teamId", teamId));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }
    }
}
