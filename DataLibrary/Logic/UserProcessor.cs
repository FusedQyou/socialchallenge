﻿using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

using static DataLibrary.DataAccess.MySqlDataAccess;

using Data;
using Data.DataForms;
using static Data.Constants;

namespace DataLibrary.Logic
{
    // Main logic for the user table
    public class UserProcessor
    {
        // Create a new user for the database
        public static int CreateUser(string username, string firstname, string insertion, string lastname, string passwordHash, string salt, AccessLevel accessLevel, int? teamId)
        {
            string query = "INSERT INTO users (username, firstname, insertion, lastname, passwordhash, salt, date_added, access_level, team_id) VALUES (@username, @firstname, @insertion, @lastname, @passwordHash, @salt, NOW(), @access_level, @team_id);";
            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@username", username));
            MySqlParameterList.Add(new MySqlParameter("@firstname", firstname));
            MySqlParameterList.Add(new MySqlParameter("@insertion", insertion));
            MySqlParameterList.Add(new MySqlParameter("@lastname", lastname));
            MySqlParameterList.Add(new MySqlParameter("@passwordHash", passwordHash));
            MySqlParameterList.Add(new MySqlParameter("@salt", salt));
            MySqlParameterList.Add(new MySqlParameter("@access_level", accessLevel));
            MySqlParameterList.Add(new MySqlParameter("@team_id", teamId));

            int? lastId;

            try {
                lastId = CreateReturnLastId(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return (int)lastId;
        }

        // Update a user by class
        public static void UpdateUserByClass(User user)
        {
            string query = "UPDATE users SET `username`=@username, `team_id`=@teamid, `firstname`=@firstname, `insertion`=@insertion, `lastname`=@lastname, `access_level`=@access_level WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@username", user.Username));
            MySqlParameterList.Add(new MySqlParameter("@firstname", user.Firstname));
            MySqlParameterList.Add(new MySqlParameter("@insertion", user.Insertion));
            MySqlParameterList.Add(new MySqlParameter("@lastname", user.Lastname));
            MySqlParameterList.Add(new MySqlParameter("@teamid", user.TeamId));
            MySqlParameterList.Add(new MySqlParameter("@access_level", user.Access_level));
            MySqlParameterList.Add(new MySqlParameter("@id", user.Id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update a user access level by id
        public static void UpdateUserAccessLevelById(int id, AccessLevel level)
        {
            string query = "UPDATE users SET `access_level`=@access_level WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@access_level", level));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update a user by class
        public static void SetUserStatusById(StatusType status, int id)
        {
            string query = "UPDATE users SET `account_status`=@account_status WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@account_status", status));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update a user by class
        public static void SetUserTeamById(int? teamId, int id)
        {
            string query = $"UPDATE users SET `team_id`=@teamid WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@teamid", teamId));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        public static List<User> LoadUsers()
        {
            return UserLoader();
        }

        public static User LoadUserByUsername(string username)
        {
            return UserLoader(username: username).FirstOrDefault();
        }

        public static User LoadUserById(int id)
        {
            return UserLoader(id: id).FirstOrDefault();
        }

        private static List<User> UserLoader(string username = null, int? id = null)
        {
            string query = @"SELECT
	                        `users`.*,
                            `teams`.`id` AS 'teamid',
                            `teams`.`name`
                        FROM `users`
                        LEFT JOIN `teams` ON `teams`.`id` = `users`.`team_id`";
            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();

            if (username != null)
            {
                query += " WHERE `username`=@username";
                MySqlParameterList.Add(new MySqlParameter("@username", username));
            }

            if (id != null)
            {
                query += " WHERE `users`.`id`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", id));
            }

            List<User> users = new List<User>();

            try
            {
                Load(query, MySqlParameterList, (MySqlDataReader reader) =>
                {
                    users.Add(new User
                    {
                        Id = reader.GetInt32("id"),
                        Username = reader.GetString("username"),
                        Firstname = reader.GetString("firstname"),
                        Insertion = reader.IsDBNull(reader.GetOrdinal("insertion")) ? null : reader.GetString("insertion"),
                        Lastname = reader.IsDBNull(reader.GetOrdinal("lastname")) ? null : reader.GetString("lastname"),
                        Date_added = reader.GetDateTime("date_added"),
                        PasswordHash = reader.GetString("passwordhash"),
                        Salt = reader.GetString("salt"),
                        TeamId = reader.IsDBNull(reader.GetOrdinal("teamid")) ? (int?)null : reader.GetInt32("teamid"),
                        Team = reader.IsDBNull(reader.GetOrdinal("teamid")) ? null : new Team
                        {
                            Id = reader.GetInt32("teamid"),
                            Name = reader.GetString("name")
                        },
                        Access_level = (AccessLevel)reader.GetInt32("access_level"),
                        Status = (StatusType)reader.GetInt32("account_status")
                    });
                });
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return users;
        }

        public static int? CountUsers()
        {
            string query = "SELECT COUNT(*) as 'total' FROM `users`";
            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            int? total = null;

            try
            {
                Load(query, MySqlParameterList, (MySqlDataReader reader) =>
                {
                    total = reader.GetInt32("total");
                });
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return total;
        }
    }
}
