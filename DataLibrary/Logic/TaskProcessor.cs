﻿using System.Collections.Generic;
using System.Linq;

using MySql.Data.MySqlClient;
using static DataLibrary.DataAccess.MySqlDataAccess;

using Data.DataForms;
using static Data.Constants;

namespace DataLibrary.Logic
{
    // Main logic for the task table
    public class TaskProcessor
    {
        // Create a new task for the database
        public static void CreateTask(int id, string name, string description)
        {
            string query = "INSERT INTO `tasks` (teamid, task, description) VALUES (@id, @name, @description);";
            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@id", id));
            MySqlParameterList.Add(new MySqlParameter("@name", name));
            MySqlParameterList.Add(new MySqlParameter("@description", description));

            try
            {
                Create(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        public static List<Task> GetTasks()
        {
            return Taskloader();
        }

        public static List<Task> LoadTeamTasksById(int teamId)
        {
            return Taskloader(teamId: teamId);
        }

        public static Task GetTasksById(int id)
        {
            return Taskloader(id: id).FirstOrDefault();
        }

        private static List<Task> Taskloader(int? id = null, int? teamId = null)
        {
            string query = @"SELECT * FROM `tasks`";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();

            if (id != null)
            {
                query += "WHERE `id`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", id));
            }

            else if (teamId != null)
            {
                query += "WHERE `teamid`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", teamId));
            }

            List<Task> tasks = new List<Task>();

            try
            {
                Load(query, MySqlParameterList, (MySqlDataReader reader) =>
                {
                    tasks.Add(new Task
                    {
                        Id = reader.GetInt32("id"),
                        TeamId = reader.GetInt32("teamid"),
                        Name = reader.GetString("Task"),
                        Description = reader.IsDBNull(reader.GetOrdinal("description")) ? null : reader.GetString("description"),
                        Done = (TaskProgress)reader.GetInt32("done"),
                        Approved = (TaskStatus)reader.GetInt32("approved")
                    });
                });
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return tasks;
        }

        // Update a task approval status
        public static void SetTaskApprovalById(int taskId, TaskStatus status)
        {
            string query = "UPDATE `tasks` SET `approved`=@approved WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@approved", status));
            MySqlParameterList.Add(new MySqlParameter("@id", taskId));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Update a task
        public static void UpdateTask(int taskId, string name, string description)
        {
            string query = "UPDATE `tasks` SET `task`=@task, `description`=@description WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@task", name));
            MySqlParameterList.Add(new MySqlParameter("@description", description));
            MySqlParameterList.Add(new MySqlParameter("@id", taskId));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

        // Mark a task as finished
        public static void SetTaskFinished(int taskId)
        {
            string query = "UPDATE `tasks` SET `done`=1 WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@id", taskId));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }
    }
}
