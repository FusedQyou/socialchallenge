﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

using static DataLibrary.DataAccess.MySqlDataAccess;
using static DataLibrary.Logic.UserProcessor;

using Data;
using Data.DataForms;
using static Data.Constants;

namespace DataLibrary.Logic
{
    // Main logic for the code table
    public class CodeProcessor
    {
        public static string CreateCode(AccessLevel accountLevel, int? teamId)
        {
            // Firstly, create two codes.
            // The first one is the oen to share. The second one is the one to store int he DB for comparison
            string code = Encrypt.GetRandomString(30, 50);
            int days = accountLevel == AccessLevel.Normal ? StudentCodeDayLimit : TeacherCodeDayLimit;

            string query = @"INSERT INTO `register_codes`
                                (id, use_by, account_level, team_id)
                            VALUES
                                (@id, @use_by, @account_level, @team_id);";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@id", code));
            MySqlParameterList.Add(new MySqlParameter("@use_by", DateTime.Now.AddDays(days).ToString("yyyy-MM-dd")));
            MySqlParameterList.Add(new MySqlParameter("@account_level", accountLevel));
            MySqlParameterList.Add(new MySqlParameter("@team_id", teamId));

            try
            {
                Create(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return code;
        }

        public static List<Code> GetCodes()
        {
            return CodeLoader();
        }

        public static Code GetCodeById(string id)
        {
            return CodeLoader(id: id).FirstOrDefault();
        }

        private static List<Code> CodeLoader(string id = null)
        {
            string query = @"SELECT
	                    `register_codes`.`id`,
                        `register_codes`.`use_by`,
                        `register_codes`.`account_level`,
                        `register_codes`.`team_id`,
                        `register_codes`.`used_by_ids`
                    FROM `register_codes`";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();

            if (id != null)
            {
                query += " WHERE `register_codes`.`id`=@id";
                MySqlParameterList.Add(new MySqlParameter("@id", id));
            }

            List<Code> codes = new List<Code>();

            try
            {
                Load(query, MySqlParameterList, (MySqlDataReader reader) =>
                {
                    List<User> userlist = new List<User>();

                    // Fill the userlist if used_by_ids is not null
                    if (!reader.IsDBNull(reader.GetOrdinal("used_by_ids")))
                    {
                        string ids = reader.GetString("used_by_ids"),
                            left,
                            right = ids;

                        while (true)
                        {
                            int index = right.IndexOf("|");
                            if (index > 0)
                            {
                                left = right.Substring(0, index);
                                right = right.Substring(index + 1, right.Length-(index+1));
                            }
                            else
                            {
                                left = right;
                            }

                            userlist.Add(LoadUserById(Convert.ToInt32(left)));

                            if (!(index > 0))
                            {
                                break;
                            }
                        }
                    }

                    codes.Add(new Code {
                        Id = reader.GetString("id"),
                        Use_By = reader.GetDateTime("use_by"),
                        Access_level = (AccessLevel)reader.GetInt32("account_level"),
                        TeamId = reader.IsDBNull(reader.GetOrdinal("team_id")) ? (int?)null : reader.GetInt32("team_id"),
                        UsedByIds = reader.IsDBNull(reader.GetOrdinal("used_by_ids")) ? null : reader.GetString("used_by_ids"),
                        Users = reader.IsDBNull(reader.GetOrdinal("used_by_ids")) ? null : userlist
                    });
                });
            }
            catch (MySqlException exception)
            {
                throw exception;
            }

            return codes;
        }

        public static void AddUserIdtoCode(string id, int userId)
        {
            Code code = GetCodeById(id);
            string userIdStrings = userId.ToString();

            if (code.UsedByIds != null)
            {
                userIdStrings += $"|{code.UsedByIds}";
            }

            string query = "UPDATE `register_codes` SET `used_by_ids`=@user_id WHERE `id`=@id";

            List<MySqlParameter> MySqlParameterList = new List<MySqlParameter>();
            MySqlParameterList.Add(new MySqlParameter("@user_id", userIdStrings));
            MySqlParameterList.Add(new MySqlParameter("@id", id));

            try
            {
                Update(query, MySqlParameterList);
            }
            catch (MySqlException exception)
            {
                throw exception;
            }
        }

    }
}
